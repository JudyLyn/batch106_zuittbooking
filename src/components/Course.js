import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//Add PropTypes in the Course component to validate its props
import PropTypes from 'prop-types';
//Apply the Proptypes function in the Course Component to make sure that the component is getting the correct property types



//let's pass a parameter in Course Component, pass a parameter or an object called course
//we need to identify/destructure the different properties that we want to pass down inside this component. 
export default function Course({course, courseId}){

//Use a state hook on the Course Component to demonstrate component state by keeping track of the number of enrollees and seats
//how do we declare state in react js
//first, we need to acquire useState()
//let's declare another variable, this variable will describe the number of "enrolled" and "seat" students inside each class/course.
//use state hook for this component to be able to store its state.
//useState(initial Value)

//syntax useState
//const [state, setState] = useState(Initial Value)
//state = Initial state (state getter)
//setState = updated State(state setter) change the value set by the initial state
	const [seats, setSeats] = useState(30); //value inside the parameter method, you can assign an initial or default value that the variable or component will be binded to
	const [count, setCount] = useState(0);

	const [isOpen, setIsOpen] = useState(true);

	const { name, description, price } = course;

//define useEffect() to have this component do something after every DOM update
//this is run automatically both after initial render for every subsequent DOM Update
//the effect hook, useEffect adds the ability to perform side "effect" function after flushing changes to the DOM. Effects are declared inside the component so they have access to its props and state
useEffect(()=>{
		if(seats === 0){
		setIsOpen(false)
	}
}, [seats]) // [seats] is an OPTIONAL parameter, react will re run this effect ONLY if any of the values contained in this array has changed from the last render /update

//life cycle component
//mounting > rendering > re rendering > unmounting
//html      >javascript  > update javascript > tinatanggal yung component

//login page,=mounting, render (load), kapag may tinype, mag r render. > unmounting, lilipat na ng ibang page, unmount component

//Single Page Application = 
//DOM Virtual DOM
//



//the state setters will be used to set a new state for our variables currently locked in their initial state
function enroll(courseId){
	//setCount(count + 1)//everytime the enroll butoon is clicked, the number of enrollees per class will increase by 1
	//setSeats(seats - 1)//while the number of seats decrease

	fetch('http://localhost:4000/users/enroll', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
		},
		body: JSON.stringify({
			courseId: courseId
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		if(data === true){
			Swal.fire({
				title: 'Success',
				icon: 'success',
				text: 'Congratulations! Your enrollment has been processed successfully'
			})
		}else{
			Swal.fire({
				title: 'Error',
				icon: 'error',
				text: 'Something went wrong!'
			})
		}
	})
}

//js synchronicity
//synchronous

	return(
	<Row>
		<Col>
			<Card className="cardCourse">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>
						<h3>Description:</h3>
						<p>
							{description}
						</p>
						<h4>Price:</h4>
						<p>PhP {price} </p>
						<h4>Seats:</h4>
						<p>{seats}</p>
						<h4>Enrollees:</h4>
						<p>{count}</p>
					</Card.Text>
					{isOpen ? <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
					: 
					<Button variant="primary" onClick={enroll} disabled>Enroll</Button>
				}
					
					
				</Card.Body>
			</Card>
		</Col>
	</Row>


		)
}


//check that the Course component is getting the correct prop types
Course.propTypes = {
	//let's call on the property to check the data
	//shape() - is used to check that a prop object conforms to a specific "shape"
	laman: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}