import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){
	// let's define state hooks for all input fields
	const history = useHistory();
	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] =useState('');

	//let's declare a variable that will describe the state of the register button component
	const [registerButton, setRegisterButton] = useState(false)

	//our next task, let us try to refactor our register user function
	useEffect(() =>{
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password !== '' && verifyPassword !== '') && (mobileNo.length === 11) && (password === verifyPassword)){
			setRegisterButton(true)

		}else{
			//here in the else, let's describe the return if any of the condition has not met
			setRegisterButton(false)
		}
	}, [firstName, lastName, email, mobileNo, password, verifyPassword])

//EXPLANATION FOR THE SCRIPT
//The values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is what is called two-way binding

//What is two way binding?
//Two data binding means The data we changed in the view(UI) has updated the state.
//The data in the state has updated the view.

//let's create a function that will simulate an actual register page
//we want to add an alert if registration is successful
	function registerUser(e){
		//let's describe the event that will happen upon registering a new user
		e.preventDefault(); //this is to avoid page redirection 

		fetch('http://localhost:4000/users/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			Swal.fire({
				title: 'Successful Registration',
				icon: 'success',
				text: 'Thank you for registering!'
			})

			history.push('/login')
		})

		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNo('');
		setPassword('');
		setVerifyPassword('');
	}

//Activity
//ensures the pages that specific user's can't access, you can create a condition to redirect them to a different page.
//if a user is logged in, make the Register page redirect to the Homepage
//push your file to gitlab
//copy and paste the link of Register.js to boodle named 49 - React js - state management

	if(user.accessToken !== null){
		return <Redirect to="/" />
	}


	return(

	<Form onSubmit={(e)=> registerUser(e)}>

		<Form.Group controlId="fName">
			<Form.Label>First Name:</Form.Label>
			<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
		</Form.Group>

		<Form.Group controlId="lName">
			<Form.Label>Last Name:</Form.Label>
			<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
		</Form.Group>


		<Form.Group controlId="userEmail">
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>
		</Form.Group>

		<Form.Group controlId="mNo">
			<Form.Label>Mobile Number:</Form.Label>
			<Form.Control type="text" placeholder="Enter 11-digit Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
		</Form.Group>

		<Form.Group controlId="password1">
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		</Form.Group>

		<Form.Group controlId="password2">
			<Form.Label>Verify Password:</Form.Label>
			<Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required/>
		</Form.Group>
		{registerButton ? <Button variant="primary" type="submit">Submit</Button> 
		:
		<Button variant="primary" type="submit" disabled>Submit</Button>
	}
		
		
	</Form>
		)

}

//ACTIVITY
//Using the code discussed earlier, update the Register page component so that it sends the new user information to localhost:4000/users via POST endpoint.
//After the successful registration, the page must redirect to the Login page. Refer to the NavBar component on how to redirect without using <Redirect />.
//Additionally, if there is a user currently logged in, the page must redirect to the home page.



/*
Add a fetch request to our register page.

    Create a new fetch request to the registerUser function.
        -add the appropriate URL
        -add the appropriate options
    Then, process the result of the fetch by using .json()
    Then, show the processed data in the console and a sweet alert that congratulates our user for registering.

    Push your file to gitlab
    copy and paste the link for Register.js to boodle named React js API integration with Fetch
*/