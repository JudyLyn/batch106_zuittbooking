import React,{ useContext, useEffect, useState } from 'react';
import Course from '../components/Course'; //This component will serve as the storage of the data upon displaying it in the browser
/*import coursesData from '../data/courses';*///to acquire the actual data that we want to display, describes all the courses records from the data folder
import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';
import DeleteButton from '../components/DeleteButton';
import UpdateButton from '../components/UpdateButton';

/*Mini- Activiy
	
	Use the UserContext to console log the global user state in this component/page
*/

export default function Courses(){

	//We will apply conditional rendering in Courses Page
	//If the user is Admin:
	//Create a new state called adminCourses
	//initial value of state is an empty array
	//if admin
	const [adminCourses, setAdminCourses] = useState([]);
	//for normal user
	const [allCourses, setAllCourses] = useState([]);


	const { user } = useContext(UserContext);
	console.log(user)
	//we added UserContext so we can use the property isAdmin for our conditional rendering


	//first, let's try to fetch allCourses from API using useEffect

		useEffect(() => {

		// use fetch() to get all the courses from API (backend)
		if (!user.isAdmin) {

			fetch("http://localhost:4000/courses/", {
				headers: { 
					Authorization: `Bearer $(user.accessToken)`
				}
			})
			.then(response => response.json())
			.then(userCourses =>{
				console.log(userCourses);

				setAllCourses(userCourses.map(enrollCourses => {
					return (

						<Course key={enrollCourses._id} course={enrollCourses} courseId={enrollCourses._id}/>
					)
				}));	

			});


		} else {

			// no auth.verify in the backend, options {} can be removed
			fetch("http://localhost:4000/courses/", {
				headers: { 
					// method: "GET",
					Authorization: `Bearer $(user.accessToken)` // user is from the declaration in line(14)
					// Authorization: `Bearer ${localStorage.getItem('token')}`
					// ^^ you can also get the token from localStorage
				}
			})
			.then(res => res.json())
			.then(activeCourses => {
				console.log(activeCourses)
				// show all data that we need for the admin
				// use state setAdminCourses ; use map()
				setAdminCourses(activeCourses.map(course => {
					// render array of react elements into our component
					return (
						<tr key={course._id}>
							<td>{course._id}</td>
							<td>{course.name}</td>
							<td>{course.description}</td>
							<td>{course.price}</td>
							<td className={course.isActive ? "text-success" : "text-danger"}>{course.isActive ? "Active" : "Inactive"}</td>
							<td><DeleteButton courseId={course._id} /></td>
							<td><UpdateButton courseId={course._id} /></td>
						</tr>
					)

				}));
			})

		}
		
	}, [])


//Activity
/*If the user is not an Admin:
    -Create a new state called allCourses.
        -initial value of state is an empty array
    -use conditional rendering, if the user is admin, show the Course Dashboard, if not, show the courses component
    -use our fetch() to get all the courses from api.
    -Map out all the courses using the state setAllCourses*/

//update your gitlab and write a commit message "Updated Courses for admin and non-admin"








	//our next task is to create/display multiple course components corresponding to the content/data from our coursesData
	//for us to be able to display all the courses from the data file we are going to use map()
	//const courseCard = coursesData.map(kahitAno =>{
		//return(
			//<Course key={kahitAno.id} laman={ kahitAno }/>//each element from the map method will be displayed
			//to keep tract the number of courses to avoid duplication, we will use the id property in key
		//)
	//})

	// conditional rendering for the UI that will appear if admin or not
	if (!user.isAdmin) {

		return (

			[allCourses]	
		)

	} else {

		return (

		<>
			<Jumbotron className="mt-5">
				<h1 className="text-center mb-5"><strong>Course Dashboard</strong></h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
							{adminCourses}
					</tbody>
				</Table>
			</Jumbotron>
		</>

		)
	}
}