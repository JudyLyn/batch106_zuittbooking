/*import logo from './logo.svg';
import Cartoon from './me.jpg';*/
import React, { useState } from 'react';
import './App.css';
import NavBar from './components/NavBar';
/*import Banner from './components/Banner';*/
import { Container } from 'react-bootstrap';
/*import Highlights from './components/Highlights';*/
//let's import react router
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import AddCourse from './pages/AddCourse';


import UserContext from './UserContext';

//Apply conditional rendering in the NavBar component such that a logout link will be shown instead of Login and Register when s user is logged in



function App() {

	//add a state hook in the App Component to set our User
	//getItem() method returns value of the specified Storage Object item
	const [user, setUser] = useState({ 
		accessToken: localStorage.getItem('accessToken'),
		isAdmin: localStorage.getItem('isAdmin') === 'true'
	});

//Finally, to logout, let's update the App.js file first and unset the user
	const unsetUser = () =>{
		localStorage.clear();
		setUser({
			accessToken: null,
			isAdmin: null
		})
	}

  return (
  	//Provider component that allows consuming components to subscribe context changes
  	//By using the built-in Provider component from the created UserContext, the given values (user and setUser) can be used anywhere in the child components without explicitly passing the values as a prop.
  	<UserContext.Provider value={{ user, setUser, unsetUser }}>
	    <Router>
		    <NavBar/>
		    <Container>
		     	<Switch>
			        <Route exact path="/" component={Home} />
			        <Route exact path="/courses" component={Courses} />
			     	<Route exact path="/login" component={Login} />
			     	<Route exact path="/register" component={Register} />
			     	<Route exact path="/addcourse" component={AddCourse} />
			     	<Route component={NotFound}/>
		        </Switch>
		    </Container>
	    </Router>
	</UserContext.Provider>
  );
}

export default App;
